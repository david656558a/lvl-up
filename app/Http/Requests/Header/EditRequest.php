<?php

namespace App\Http\Requests\Header;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'description' => 'required',
            'image' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please write Title',
            'description.required' => 'Please write Description',
        ];
    }
}
