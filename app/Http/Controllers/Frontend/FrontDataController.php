<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use App\Models\ContactUsPrice;
use App\Models\Header;
use App\Models\OurWork;
use App\Models\Question;
use App\Models\WhatWeDo;
use App\Models\WhoWeAre;
use Illuminate\Http\Request;

class FrontDataController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function home()
    {
        $header = Header::query()->first();
        $whatWeDos = WhatWeDo::all();
        $ourWorks = OurWork::query()->inRandomOrder(3)->select('path', 'title')->get();
        $whoWeAre = WhoWeAre::query()->first();
        $test = OurWork::findOrFail(1);
        return view('frontend.pages.test.test', compact('header', 'whatWeDos', 'ourWorks', 'whoWeAre', 'test'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function services()
    {
        $whatWeDos = WhatWeDo::all();
        return view('', compact('whatWeDos'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function whoWeAre()
    {
        $whoWeAre = WhoWeAre::query()->first();
        $whatWeDos = WhatWeDo::all();
        $questions = Question::all();
        return view('', compact('whoWeAre', 'whatWeDos', 'questions'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function ourWork()
    {
        $ourWorks = OurWork::all();
        return view('', compact('ourWorks'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function ourWorkSingle($id)
    {
        $ourWork = OurWork::findOrFail($id);
        return view('', compact('ourWork'));
    }

    /**
     * @param $level
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function contactUsView($level)
    {
        switch ($level) {
            case 1:
                return view('');
                break;
            case 2:
                return view('');
                break;
            case 3:
                return view('');
                break;
            case 4:
                return view('');
                break;
            case 5:
                return view('');
                break;
            case 6:
                $price = ContactUsPrice::all();
                return view('', compact('price'));
                break;
            case 7:
                return view('');
                break;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function contactUs(Request $request)
    {
        $contactUs = new ContactUs();
        $contactUs->name = $request->name;
        $contactUs->company = $request->company;
        $contactUs->email = $request->email;
        $contactUs->number = $request->number;
        $contactUs->project = $request->project;
        $contactUs->contact_us_price_id = $request->contact_us_price_id;
        $contactUs->save();

        return redirect()->route('home');
    }


}
