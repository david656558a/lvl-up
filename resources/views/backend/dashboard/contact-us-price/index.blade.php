@extends('layouts.dashboard')


@section('title')
    <title>Contact us price</title>
@endsection

@section('css')

    <style>
        .custom-scroll{
            display: block;
            height: 100px !important;
            overflow-y: scroll;
        }
    </style>
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Contact us Price</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                            <li class="breadcrumb-item active">Contact us Price</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div>
                                <a href="{{route('price.create')}}" class="btn btn-success" style="float: right; margin: 15px">Create</a>
                            </div>
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Price</th>
                                        <th>Access</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($prices as $item)
                                            <tr>
                                                <td>{{$item->price}}</td>
                                                <td>
                                                    <div class="d-flex">
                                                        <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("price.edit", $item->id) }}" class="btn btn-primary edit-access"><i  class="fas fa-edit" ></i></a>
                                                        <form method="POST" action="{{ route("price.destroy" , $item->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <a href="javascript:;" onclick="return confirm('Are you sure you want to delete this item?');">
                                                                <button type="submit" style="padding: 0px 5px;" class="ml-2 btn btn-danger" >
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </a>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <div style="margin: 0 auto">
                                {{ $prices->links('pagination::bootstrap-4') }}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')


@endsection
