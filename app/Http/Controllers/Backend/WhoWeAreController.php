<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Hellper\Hellper;
use App\Http\Requests\WhoWeAre\CreateRequest;
use App\Http\Requests\WhoWeAre\EditRequest;
use App\Models\WhoWeAre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class WhoWeAreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $whoWeAres = WhoWeAre::query()->orderBy('id', 'DESC')->paginate(10);
        return view('backend.dashboard.who-we-are.index',compact('whoWeAres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.who-we-are.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        DB::beginTransaction();
        $whoWeAre = new WhoWeAre();
        $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'who-we-are' . DIRECTORY_SEPARATOR);
        $name =  Hellper::upload($request->image, $path);
        $image = "/storage/who-we-are/" . $name;
        $whoWeAre->path = $image;
        $whoWeAre->sub_description = $request->sub_description;
        $whoWeAre->description = $request->description;
        $whoWeAre->save();

        DB::commit();

        return redirect()->route('who-we-are.index')->with('message', 'Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $whoWeAre = WhoWeAre::findOrFail($id);
        return view('backend.dashboard.who-we-are.edit',compact('whoWeAre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {
        DB::beginTransaction();
        $whoWeAre = WhoWeAre::findOrFail($id);
        $whoWeAre->sub_description = $request->sub_description;
        $whoWeAre->description = $request->description;

        if ($request->image){
            $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'who-we-are' . DIRECTORY_SEPARATOR);
            $PathForDelete = str_replace('/storage', '', $whoWeAre->path);
            Storage::delete('/public' . $PathForDelete);
            $name =  Hellper::upload($request->image, $path);
            $image = "/storage/who-we-are/" . $name;
            $whoWeAre->path = $image;
        }
        $whoWeAre->save();

        DB::commit();

        return redirect()->route('who-we-are.index')->with('message', 'Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $whoWeAre = WhoWeAre::findOrFail($id);
        $PathForDelete = str_replace('/storage', '', $whoWeAre->path);
        Storage::delete('/public' . $PathForDelete);
        $whoWeAre->delete();
        return redirect()->route('who-we-are.index')->with('message', 'Successfully');
    }
}
