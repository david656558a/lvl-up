@extends('layouts.dashboard')


@section('title')
    <title>Create</title>
@endsection

@section('css')

@endsection



@section('dashboard')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Create</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>


    <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Who We Are</h3>
                            </div>
                            <form action="{{route('what-we-do.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body row">
                                    <div class="form-group col-6">
                                        <label>Title</label>
                                        <input class="form-control" type="text" name="title" placeholder="Slug title" value="{{ old('title') }}">
                                    </div>
                                    <div class="form-group col-12">
                                        <label>Description</label>
                                        <textarea class="summernote" name="description">{{ old('description') }}</textarea>
                                    </div>
                                    <div class="form-group closest-img-files-one-custom">
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">Icon</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input img-files-one-custom-input" name="image_icon" >
                                                    <label class="custom-file-label" for="imgfilesOneCustom">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-view-one-custom">

                                        </div>
                                    </div>
                                    <div class="form-group closest-img-files-one-custom">
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">Images</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input img-files-one-custom-input" name="image" >
                                                    <label class="custom-file-label" for="imgfilesOneCustom">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-view-one-custom">

                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
    <!-- /.content -->
</div>
@endsection

@section('js')

@endsection


