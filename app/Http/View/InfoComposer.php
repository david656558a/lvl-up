<?php


namespace App\Http\View;

use App\Models\UserInfo;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;


class InfoComposer
{
    public function __construct()
    {
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = UserInfo::query()->first();
        $view->with('InfoGLOBAL', $data);
    }
}
