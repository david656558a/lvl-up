<?php

namespace App\Http\Requests\WhatWeDo;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image_icon' => 'required',
            'title' => 'required|max:255',
            'description' => 'required',
            'image' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'image_icon.required' => 'Please write Icon',
            'title.required' => 'Please write Title',
            'description.required' => 'Please write Description',
            'image.required' => 'Please write Image',
        ];
    }
}
