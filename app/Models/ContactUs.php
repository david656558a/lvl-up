<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    use HasFactory;

    protected $table = 'contact_us';
    protected $fillable = [
        'name',
        'company',
        'email',
        'number',
        'project',
        'contact_us_price_id',
    ];

    public function price()
    {
        return $this->belongsTo(ContactUsPrice::class);
    }


}
