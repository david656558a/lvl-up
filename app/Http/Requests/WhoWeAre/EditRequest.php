<?php

namespace App\Http\Requests\WhoWeAre;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sub_description' => 'required',
            'description' => 'required',
            'image' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'sub_description.required' => 'Please write Title',
            'description.required' => 'Please write Description',
        ];
    }
}
