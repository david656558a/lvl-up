@extends('layouts.dashboard')


@section('title')
    <title>Edit</title>
@endsection

@section('css')
@endsection

@section('dashboard')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">User info</h3>
                        </div>
                        <form action="{{route('user-info.update', $userInfo->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="card-body row">
                                <div class="form-group col-6">
                                    <label>Number</label>
                                    <input class="form-control" type="number" name="number" placeholder="Slug title" value="{{ $userInfo->number }}">
                                </div>
                                <div class="form-group col-6">
                                    <label>Email</label>
                                    <input class="form-control" type="email" name="email" placeholder="Slug Description"  value="{{ $userInfo->email }}">
                                </div>
                                <div class="form-group col-6">
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="linkedin" placeholder="Slug Description"  value="{{ $userInfo->linkedin }}">
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('js')

@endsection


