<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Hellper\Hellper;
use App\Http\Requests\OurWork\CreateRequest;
use App\Http\Requests\OurWork\EditRequest;
use App\Models\OurWork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class OurWorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $ourWorks = OurWork::query()->orderBy('id', 'DESC')->paginate(10);
        return view('backend.dashboard.our-work.index',compact('ourWorks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.our-work.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        DB::beginTransaction();
        $ourWorks = new OurWork();
        $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'our-work' . DIRECTORY_SEPARATOR);
        $name =  Hellper::upload($request->image, $path);
        $image = "/storage/our-work/" . $name;
        $ourWorks->path = $image;
        $ourWorks->title = $request->title;
        $ourWorks->description = $request->description;
        $ourWorks->save();

        DB::commit();

        return redirect()->route('our-work.index')->with('message', 'Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $ourWork = OurWork::findOrFail($id);
        return view('backend.dashboard.our-work.edit',compact('ourWork'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {
        DB::beginTransaction();
        $ourWorks = OurWork::findOrFail($id);
        $ourWorks->title = $request->title;
        $ourWorks->description = $request->description;

        if ($request->image){
            $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'our-work' . DIRECTORY_SEPARATOR);
            $PathForDelete = str_replace('/storage', '', $ourWorks->path);
            Storage::delete('/public' . $PathForDelete);
            $name =  Hellper::upload($request->image, $path);
            $image = "/storage/our-work/" . $name;
            $ourWorks->path = $image;
        }
        $ourWorks->save();

        DB::commit();

        return redirect()->route('our-work.index')->with('message', 'Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $ourWorks = OurWork::findOrFail($id);
        $PathForDelete = str_replace('/storage', '', $ourWorks->path);
        Storage::delete('/public' . $PathForDelete);
        $ourWorks->delete();
        return redirect()->route('our-work.index')->with('message', 'Successfully');
    }
}
