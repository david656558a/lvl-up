let intro = $(".intro");

$(function() {
    //get the welcome msg element
    var $all_msg = $(".logo-header span");
    //get a list of letters from the welcome text
    var $wordList = $('.logo-header span').text().split("");
    //clear the welcome text msg
    $('.logo-header span').text("");
    //loop through the letters in the $wordList array
    $.each($wordList, function(idx, elem) {
        //create a span for the letter and set opacity to 0
        var newEL = $("<span/>").text(elem).css({
            opacity: 0
        });
        //append it to the welcome message
        newEL.appendTo($all_msg);
        //set the delay on the animation for this element
        newEL.delay(idx * 70);
        //animate the opacity back to full 1
        newEL.animate({
            opacity: 1
        }, 1700);
    });

});

$(function() {
    //get the welcome msg element
    var $all_msg = $(".logo-footer span");
    //get a list of letters from the welcome text
    var $wordList = $('.logo-footer span').text().split("");
    //clear the welcome text msg
    $('.logo-footer span').text("");
    //loop through the letters in the $wordList array
    $.each($wordList, function(idx, elem) {
        //create a span for the letter and set opacity to 0
        var newEL = $("<span/>").text(elem).css({
            opacity: 0
        });
        //append it to the welcome message
        newEL.appendTo($all_msg);
        //set the delay on the animation for this element
        newEL.delay(idx * 70);
        //animate the opacity back to full 1
        newEL.animate({
            opacity: 1
        }, 1700);
    });

});

window.addEventListener('DOMContentLoaded', ()=>{
    setTimeout(()=> {
        intro.css("right", "-100vw");
    }, 2800);
})