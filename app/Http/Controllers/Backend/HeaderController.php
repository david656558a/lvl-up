<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Hellper\Hellper;
use App\Http\Requests\Header\CreateRequest;
use App\Http\Requests\Header\EditRequest;
use App\Models\Header;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $header = Header::query()->first();
        return view('backend.dashboard.header.index',compact('header'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.header.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        DB::beginTransaction();
        $header = new Header();
        $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'header' . DIRECTORY_SEPARATOR);
        $name =  Hellper::upload($request->image, $path);
        $image = "/storage/header/" . $name;
        $header->path = $image;
        $header->title = $request->title;
        $header->description = $request->description;
        $header->save();

        DB::commit();

        return redirect()->route('header.index')->with('message', 'Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $header = Header::findOrFail($id);
        return view('backend.dashboard.header.edit',compact('header'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {
        DB::beginTransaction();
        $header = Header::findOrFail($id);
        $header->title = $request->title;
        $header->description = $request->description;

        if ($request->image){
            $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'header' . DIRECTORY_SEPARATOR);
            $PathForDelete = str_replace('/storage', '', $header->path);
            Storage::delete('/public' . $PathForDelete);
            $name =  Hellper::upload($request->image, $path);
            $image = "/storage/header/" . $name;
            $header->path = $image;
        }
        $header->save();

        DB::commit();

        return redirect()->route('header.index')->with('message', 'Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
//        $header = Header::findOrFail($id);
//        $PathForDelete = str_replace('/storage', '', $header->path);
//        Storage::delete('/public' . $PathForDelete);
//        $header->delete();
//        return redirect()->route('category.index')->with('message', 'Successfully');
    }
}
