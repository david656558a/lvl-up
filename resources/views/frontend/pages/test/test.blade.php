<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!--main css-->
    <link rel="stylesheet" href="{{asset('assets/frontend/css/main.css')}}">

    <title>LEVEL UP Digital Solutions</title>
</head>
<body>
<!--  intro logo-->
<div class="intro">
    <div class="intro-logo">
        <div class="logo-image"><img src="{{asset('assets/frontend/images/intro-logo.png')}}" alt="logo"></div>
        <h1 class="logo-header">
            <span>LEVEL UP</span>
        </h1>
        <h1 class="logo-footer">
            <span>Digital Solutions</span>
        </h1>
    </div>
</div>

<!--  side linkedIn navbar-->
<div class="sidenav">
    <div class="linkedIn-link"><a href="#">Linkedin</a></div>
</div>

<!-- header-->
<header>
    <div class="page-container">
        <div class="header-content">
            <div class="header-logo"><img src="{{asset('assets/frontend/images/main-logo.png')}}" alt="header logo"></div>
            <div class="header-contact">
                <div class="item">
                    <a href="tel:+1 000 000 0000">
                        <img src="{{asset('assets/frontend/images/phone.png')}}" alt="phone">
                        +1 000 000 0000
                    </a>
                </div>
                <div class="item">
                    <a href="mailto:lud@info.agency">
                        <img src="{{asset('assets/frontend/images/mail.png')}}" alt="mail">
                        lud@info.agency
                    </a>
                </div>
                <div class="item">
                    <div class="navbar-toggle">
                        <img src="{{asset('assets/frontend/images/navbar-toggle.png')}}" alt="navbar toggle">
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!--  first section-->
<section>
    <div class="first-section">
        <div class="page-container">
            <div class="content">
                <div class="text">
                    <h1 class="main-title">Lorem Ipsum is simply dummy text of the</h1>
                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
                    <button class="start-project-btn dark-blue">Start A Project</button>
                </div>
                <div class="image">
                    <img src="{{asset('assets/frontend/images/image.png')}}" alt="image">
                </div>
            </div>
        </div>
    </div>
</section>

<!--second section-->
<section>
    <div class="whatWeDo-section">
        <div class="page-container">
            <div class="main-title">What We Do</div>
            <div class="works-list">
                <div class="work-item">
                    <img src="{{asset('assets/frontend/images/branding.png')}}" alt="branding">
                    <span>Branding</span>
                </div>
                <div class="work-item">
                    <img src="{{asset('assets/frontend/images/data.png')}}" alt="data">
                    <span>Website Development</span>
                </div>
                <div class="work-item">
                    <img src="{{asset('assets/frontend/images/social-media.png')}}" alt="social-media">
                    <span>Social Media Management</span>
                </div>
                <div class="work-item">
                    <img src="{{asset('assets/frontend/images/content-writing.png')}}" alt="content-writing">
                    <span>Content Writing</span>
                </div>
                <div class="work-item">
                    <img src="{{asset('assets/frontend/images/seo.png')}}" alt="seo">
                    <span>SEO/ASO & PPC</span>
                </div>
                <div class="work-item">
                    <img src="{{asset('assets/frontend/images/video-camera.png')}}" alt="video-camera">
                    <span>Videography & Photography</span>
                </div>
                <div class="work-item">
                    <img src="{{asset('assets/frontend/images/profit.png')}}" alt="profit">
                    <span>Marketing Campaigns</span>
                </div>
                <div class="work-item">
                    <img src="{{asset('assets/frontend/images/app-development.png')}}" alt="app-development">
                    <span>App Development</span>
                </div>
            </div>
        </div>
    </div>
</section>

<!--second section-->
<section>
    <div class="whatWeDo-section">
        <div class="page-container">
            <div class="main-title">Test</div>
            <div class="works-list">

{{--                {!! $test->description !!}--}}
            <?php echo $test->description ?>


            </div>
        </div>
    </div>
</section>

</body>

<script
    src="https://code.jquery.com/jquery-3.6.4.js"
    integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E="
    crossorigin="anonymous"></script>
<script src="{{asset('assets/frontend/js/script.js')}}"></script>
</html>
