<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactUsPrice extends Model
{
    use HasFactory;

    protected $table = 'contact_us_price';
    protected $fillable = [
        'price',
    ];


    public function contactus()
    {
        return $this->hasOne(ContactUs::class,'contact_us_price_id','id');
    }
}
