@extends('layouts.dashboard')


@section('title')
    <title>Edit</title>
@endsection

@section('css')
@endsection

@section('dashboard')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">Question</h3>
                        </div>
                        <form action="{{route('question.update', $question->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="card-body row">
                                <div class="form-group col-6">
                                    <label>Title</label>
                                    <input class="form-control" type="text" name="title" placeholder="Slug title" value="{{ $question->title }}">
                                </div>
                                <div class="form-group col-12">
                                    <label>Description</label>
                                    <textarea class="summernote" name="description">{{ $question->description }}</textarea>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('js')

@endsection


