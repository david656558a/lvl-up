<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserInfo\CreateRequest;
use App\Http\Requests\UserInfo\EditRequest;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $userInfo = UserInfo::query()->first();
        return view('backend.dashboard.user-info.index',compact('userInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        DB::beginTransaction();
        $userInfo = new UserInfo();
        $userInfo->user_id = auth()->id();
        $userInfo->number = $request->number;
        $userInfo->email = $request->email;
        $userInfo->linkedin = $request->linkedin;
        $userInfo->save();

        DB::commit();

        return redirect()->route('user-info.index')->with('message', 'Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $userInfo = UserInfo::findOrFail($id);
        return view('backend.dashboard.user-info.edit',compact('userInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {
//        dd($request->all());
//        dd(auth()->id());
        DB::beginTransaction();
        $userInfo = UserInfo::findOrFail($id);
        $userInfo->user_id = auth()->id();
        $userInfo->number = $request->number;
        $userInfo->email = $request->email;
        $userInfo->linkedin = $request->linkedin;
        $userInfo->save();

        DB::commit();

        return redirect()->route('user-info.index')->with('message', 'Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
//        $question = UserInfo::findOrFail($id);
//        $question->delete();
//        return redirect()->route('user-info.index')->with('message', 'Successfully');
    }
}
