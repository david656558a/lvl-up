<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Hellper\Hellper;
use App\Http\Requests\WhatWeDo\CreateRequest;
use App\Http\Requests\WhatWeDo\EditRequest;
use App\Models\WhatWeDo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class WhatWeDoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $whatWeDos = WhatWeDo::query()->orderBy('id', 'DESC')->paginate(10);
        return view('backend.dashboard.what-we-do.index',compact('whatWeDos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.what-we-do.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
//        dd($request->all());
        DB::beginTransaction();
        $whatWeDo = new WhatWeDo();
        $pathIcon = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'icon' . DIRECTORY_SEPARATOR);
        $pathIcon =  Hellper::upload($request->image_icon, $pathIcon);
        $imageIcon = "/storage/icon/" . $pathIcon;
        $whatWeDo->icon = $imageIcon;

        $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'what-we-do' . DIRECTORY_SEPARATOR);
        $name =  Hellper::upload($request->image, $path);
        $image = "/storage/what-we-do/" . $name;
        $whatWeDo->path = $image;

        $whatWeDo->title = $request->title;
        $whatWeDo->description = $request->description;
        $whatWeDo->save();



        DB::commit();


        return redirect()->route('what-we-do.index')->with('message', 'Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $whatWeDo = WhatWeDo::findOrFail($id);
        return view('backend.dashboard.what-we-do.edit',compact('whatWeDo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {
        DB::beginTransaction();
        $whatWeDo = WhatWeDo::findOrFail($id);
        $whatWeDo->title = $request->title;
        $whatWeDo->description = $request->description;

        if (isset($request->image_icon)){
            $pathIcon = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'icon' . DIRECTORY_SEPARATOR);
            $PathForDeleteIcon = str_replace('/storage', '', $whatWeDo->icon);
            Storage::delete('/public' . $PathForDeleteIcon);
            $nameIcon =  Hellper::upload($request->image_icon, $pathIcon);
            $imageIcon = "/storage/icon/" . $nameIcon;
            $whatWeDo->icon = $imageIcon;
        }

        if (isset($request->image)){
            $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'what-we-do' . DIRECTORY_SEPARATOR);
            $PathForDelete = str_replace('/storage', '', $whatWeDo->path);
            Storage::delete('/public' . $PathForDelete);
            $name =  Hellper::upload($request->image, $path);
            $image = "/storage/what-we-do/" . $name;
            $whatWeDo->path = $image;
        }

        $whatWeDo->save();

        DB::commit();

        return redirect()->route('what-we-do.index')->with('message', 'Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $whatWeDo = WhatWeDo::findOrFail($id);
        $PathForDelete1 = str_replace('/storage', '', $whatWeDo->path);
        Storage::delete('/public' . $PathForDelete1);
        $PathForDelete = str_replace('/storage', '', $whatWeDo->icon);
        Storage::delete('/public' . $PathForDelete);
        $whatWeDo->delete();
        return redirect()->route('what-we-do.index')->with('message', 'Successfully');
    }
}
