<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUsPrice\CreateRequest;
use App\Http\Requests\ContactUsPrice\EditRequest;
use App\Models\ContactUsPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactUsPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $prices = ContactUsPrice::query()->orderBy('id', 'DESC')->paginate(10);
        return view('backend.dashboard.contact-us-price.index',compact('prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.contact-us-price.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        DB::beginTransaction();
        $price = new ContactUsPrice();
        $price->price = $request->price;
        $price->save();

        DB::commit();

        return redirect()->route('price.index')->with('message', 'Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $price = ContactUsPrice::findOrFail($id);
        return view('backend.dashboard.contact-us-price.edit',compact('price'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {
        DB::beginTransaction();
        $price = ContactUsPrice::findOrFail($id);
        $price->price = $request->price;
        $price->save();

        DB::commit();

        return redirect()->route('price.index')->with('message', 'Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $price = ContactUsPrice::findOrFail($id);
        $price->delete();
        return redirect()->route('price.index')->with('message', 'Successfully');
    }
}
