<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{asset('assets/backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Admin Panel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
{{--                <img src="{{asset('assets/backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">--}}
            </div>
            <div class="info">
                <a href="/" class="d-block">Go to Web site</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                {{--<li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-envelope"></i>
                        <p>
                            Droup Down
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Inbox</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Compose</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Read</p>
                            </a>
                        </li>
                    </ul>
                </li>--}}





{{--                <li class="nav-item has-treeview {{ Route::currentRouteNamed(['category.index', 'category.edit', 'category.create',
                                                                            'word.index', 'word.edit', 'word.create',
                                                                            'sub-category.index', 'sub-category.edit', 'sub-category.create',
                                                                            'group.index', 'group.edit', 'group.create']) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ Route::currentRouteNamed(['category.index', 'category.edit', 'category.create',
                                                                            'word.index', 'word.edit', 'word.create',
                                                                            'sub-category.index', 'sub-category.edit', 'sub-category.create',
                                                                            'group.index', 'group.edit', 'group.create']) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Vocabulary
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('category.index')}}" class="nav-link {{ Route::currentRouteNamed(['category.index', 'category.edit', 'category.create']) ? 'active' : '' }}">
                                --}}{{--                                <i class="nav-icon far fa-plus-square"></i>--}}{{--
                                <i class="far fa-circle nav-icon"></i>
                                <p>Category</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('sub-category.index')}}" class="nav-link {{ Route::currentRouteNamed(['sub-category.index', 'sub-category.edit', 'sub-category.create']) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                --}}{{--                                <p>Sub Category</p>--}}{{--
                                <p>Group</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('group.index')}}" class="nav-link {{ Route::currentRouteNamed(['group.index', 'group.edit', 'group.create']) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                --}}{{--                                <p>Group</p>--}}{{--
                                <p>Subgroup</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('word.index')}}" class="nav-link {{ Route::currentRouteNamed(['word.index', 'word.edit', 'word.create']) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Word</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{route('language.index')}}" class="nav-link {{ Route::currentRouteNamed(['language.index', 'language.edit', 'language.create']) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-language"></i>
                        <p>
                            Languages
                        </p>
                    </a>
                </li>--}}
                <li class="nav-item">
                    <a href="{{route('header.index')}}" class="nav-link {{ Route::currentRouteNamed(['header.index', 'header.edit', 'header.create']) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-heading"></i>
                        <p>
                            Header
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('what-we-do.index')}}" class="nav-link {{ Route::currentRouteNamed(['what-we-do.index', 'what-we-do.edit', 'what-we-do.create']) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            What we do
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('our-work.index')}}" class="nav-link {{ Route::currentRouteNamed(['our-work.index', 'our-work.edit', 'our-work.create']) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-network-wired"></i>
                        <p>
                            Our work
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('who-we-are.index')}}" class="nav-link {{ Route::currentRouteNamed(['who-we-are.index', 'who-we-are.edit', 'who-we-are.create']) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-file"></i>
                        <p>
                            Who we are
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('question.index')}}" class="nav-link {{ Route::currentRouteNamed(['question.index', 'question.edit', 'question.create']) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Question
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('user-info.index')}}" class="nav-link {{ Route::currentRouteNamed(['user-info.index', 'user-info.edit', 'user-info.create']) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            User info
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('price.index')}}" class="nav-link {{ Route::currentRouteNamed(['price.index', 'price.edit', 'price.create']) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-hand-holding-usd"></i>
                        <p>
                            Contact us price
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('price.index')}}" class="nav-link {{ Route::currentRouteNamed(['price.index', 'price.edit', 'price.create']) ? 'active' : '' }}">
                        <i class="nav-icon  fas fa-address-card"></i>
                        <p>
                            Contact us Request
                        </p>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
