@if(Session::has('message'))
    <div class="container" style="margin-top: 15px;">
        <div class="alert alert-success">
            <em>{!! session('message') !!}</em>
        </div>
    </div>
@endif
@if(Session::has('error'))
    <div class="container" style="margin-top: 15px;">
        <div class="alert alert-danger">
            <em>{!! session('error') !!}</em>
        </div>
    </div>
@endif

@if (count($errors) > 0)
    <div class="container" style="margin-top: 15px;">
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <em>{{ $error }}</em> <br>
            @endforeach
        </div>
    </div>
@endif


