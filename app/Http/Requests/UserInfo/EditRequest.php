<?php

namespace App\Http\Requests\UserInfo;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required',
            'email' => 'required',
            'linkedin' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'number.required' => 'Please write number',
            'email.required' => 'Please write email',
        ];
    }
}
