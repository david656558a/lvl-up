<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::group(['middleware'=> 'auth','prefix' => '/admin', 'namespace' => 'App\Http\Controllers\Backend'], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('/header', HeaderController::class);
    Route::resource('/what-we-do', WhatWeDoController::class);
    Route::resource('/our-work', OurWorkController::class);
    Route::resource('/who-we-are', WhoWeAreController::class);
    Route::resource('/question', QuestionController::class);
    Route::resource('/user-info', UserInfoController::class);
    Route::resource('/contact-us/price', ContactUsPriceController::class);
});



Route::group(['namespace' => 'App\Http\Controllers\Frontend'], function() {
    Route::get('/home', 'FrontDataController@home')->name('home');
    Route::get('/services', 'FrontDataController@services')->name('services');
    Route::get('/who-we-are', 'FrontDataController@whoWeAre')->name('who.we.are');
    Route::get('/our-work', 'FrontDataController@ourWork')->name('our.work');
    Route::get('/our-work/{id}', 'FrontDataController@ourWorkSingle')->name('our.work.single');
    Route::get('/contact-us/{level}', 'FrontDataController@contactUsView')->name('contact.us');
    Route::post('/contact-us', 'FrontDataController@contactUs')->name('contact.us.form');
});
